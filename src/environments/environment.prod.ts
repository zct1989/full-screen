export const environment = {
  SERVER_URL: 'http://10.173.112.129:9100/api',
  inEventSource: 'http://10.173.112.129:9100/api/ABDoorController/inPush',
  outEventSource: 'http://10.173.112.129:9100/api/ABDoorController/outPush',
  production: true,
  useHash: true,
  hmr: false,
};
