export const apiController = {
  login: {
    controller: 'operator',
    action: 'login',
    method: 'POST',
  },
  lateCarousel: {
    controller: 'attendanceRecordController',
    action: 'lateCarousel',
    method: 'GET',
  },
  queryTodayAccessRecord: {
    controller: 'accessRecordController',
    action: 'queryTodayAccessRecord',
    method: 'GET',
  },
  queryAmt: {
    controller: 'attendanceRecordController',
    action: 'queryAmt',
    method: 'GET',
  },
  queryInner: {
    controller: 'ABDoorController',
    action: 'query',
    method: 'GET',
  },
};
