import { Component, OnInit, ElementRef } from '@angular/core';
import { ApiService } from '@app/services/api.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-dashboard-a',
  templateUrl: './dashboard-a.component.html',
  styleUrls: ['./dashboard-a.component.scss'],
  providers: [ApiService],
})
export class DashboardAComponent implements OnInit {
  public lateList: any[] = [];
  public lateInfo: any = {};
  public today = new Date();
  public title = '8:00 未到岗人员';
  public hour = 1;
  public title2 = '8:00 未到岗人数';

  constructor(private elRef: ElementRef, private apiService: ApiService) {}

  ngOnInit() {
    timer(1000, 1000 * 60).subscribe(() => {
      this.getLateUser();
      this.getLateInfo();
      this.today = new Date();
      this.hour = new Date().getHours();
      // tslint:disable-next-line: prefer-conditional-expression
      if (this.hour < 12) {
        this.title = '8:00 以后到岗人员';
        this.title2 = '8:00 以后到岗人员';
      } else {
        this.title = '18:00 前离岗人员';
        this.title2 = '18:00 前离岗人数';
      }
    });
  }

  public getLateUser() {
    this.apiService.lateCarousel().subscribe(data => {
      this.lateList = data;
      const container = this.elRef.nativeElement;

      setTimeout(
        () =>
          container.scrollTo({
            top: container.scrollHeight,
            behavior: 'smooth',
          }),
        100,
      );
    });
  }

  public getLateInfo() {
    this.apiService.queryAmt().subscribe(data => {
      this.lateInfo = data;
    });
  }
}
