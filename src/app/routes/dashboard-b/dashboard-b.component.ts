import { Component, OnInit } from '@angular/core';
import { ApiService } from '@app/services/api.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-dashboard-b',
  templateUrl: './dashboard-b.component.html',
  styleUrls: ['./dashboard-b.component.scss'],
})
export class DashboardBComponent implements OnInit {
  public today = new Date();
  public innerItems: any[] = [];
  constructor(private apiService: ApiService) { }

  public innerSort = [
    '一监区',
    '二监区',
    '三监区',
    '四监区',
    '五监区',
    '六监区',
    '八监区',
    '七监区',
    '九监区',
    '十监区',
    '十一监区',
    '十二监区',
    '特勤队',
    '医院',
    '机关楼',
    '会见中心',
    '外协',
    '狱内总数',
  ];

  ngOnInit() {
    timer(1000, 1000 * 60).subscribe(() => {
      this.getInnerList();
      this.today = new Date();
    });
  }

  getInnerList() {
    this.apiService.queryInner().subscribe(data => {
      this.innerItems = Object.entries(data)
        .map(([key, value]) => ({
          label: key,
          count: value,
        }))
        .sort((x, y) => this.innerSort.indexOf(x.label) - this.innerSort.indexOf(y.label));
      this.innerItems.pop();
      this.innerItems.pop();
      let all = 0;
      this.innerItems.forEach((val) => {
        all += val.count
      })
      this.innerItems.push({ label: "狱内总数", count: all })
      console.log(this.innerItems)
    });
  }
}
