import { NetService, PageService } from '@core/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { apiController } from '@app/config/service/api.controller';

@Injectable()
export class ApiService {
  constructor(private net: NetService) {}

  public login(params): Observable<any> {
    return this.net.send({
      service: apiController.login,
      params,
    });
  }

  public lateCarousel(): Observable<any> {
    return this.net.send({
      service: apiController.lateCarousel,
    });
  }

  public queryAmt(): Observable<any> {
    return this.net.send({
      service: apiController.queryAmt,
    });
  }

  public queryTodayAccessRecord() {
    return this.net.send({
      service: apiController.queryTodayAccessRecord,
    });
  }

  public queryInner() {
    return this.net.send({
      service: apiController.queryInner,
    });
  }
}
